#!/bin/bash

mvn clean install

docker run -e MYSQL_ROOT_PASSWORD=supersecret -e MYSQL_DATABASE=product -p 3305:3306 -d --name mysql mysql:5.7

sleep 10.0

java -jar target/supplier-api-0.0.1-SNAPSHOT.jar
