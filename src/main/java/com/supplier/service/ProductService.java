package com.supplier.service;

import com.supplier.controller.request.CreateProductRequest;
import com.supplier.controller.request.UpdateProductRequest;
import com.supplier.controller.response.ProductResponse;
import com.supplier.entity.Product;
import com.supplier.exception.ProductNotFound;
import com.supplier.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    public List<ProductResponse> getAll() {
        return repository.findAll()
                .stream()
                .map(ProductResponse::of)
                .collect(Collectors.toList());
    }

    @SneakyThrows(ProductNotFound.class)
    public ProductResponse getById(Long productId){
        return repository.findById(productId)
                .map(ProductResponse::of)
                .orElseThrow(() -> new ProductNotFound("Product not found."));
    }

    public Product create(CreateProductRequest request) {
        Product product = Product.of(request);
        return repository.save(product);
    }

    @SneakyThrows(ProductNotFound.class)
    public Product update(Long productId, UpdateProductRequest request) {
        Optional<Product> productOpt = repository.findById(productId);
        if(!productOpt.isPresent()) {
            throw new ProductNotFound("Product doesnt exists.");
        }
        Product product = productOpt.get();
        if(Objects.nonNull(request.getName())) {
            product.setName(request.getName());
        }

        if(Objects.nonNull(request.getUnitPrice())) {
            product.setUnitPrice(request.getUnitPrice());
        }
        product.setUpdateDate(LocalDateTime.now());
        return repository.save(product);
    }

    @SneakyThrows(ProductNotFound.class)
    public void delete(Long productId) {
        if(!repository.existsById(productId)) {
            throw new ProductNotFound("Product doesnt exists.");
        }
        repository.deleteById(productId);
    }

    @SneakyThrows(ProductNotFound.class)
    public void updateQuantity(Long productId, Long quantity) {
        Optional<Product> productOpt = repository.findById(productId);
        if(!productOpt.isPresent()) {
            throw new ProductNotFound("Product doesnt exists.");
        }
        Product product = productOpt.get();
        product.setQuantityInStock(quantity);

        repository.save(product);
    }
}
