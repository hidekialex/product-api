package com.supplier.controller;

import com.supplier.controller.request.CreateProductRequest;
import com.supplier.controller.request.UpdateProductRequest;
import com.supplier.controller.response.HrefResponse;
import com.supplier.controller.response.ProductResponse;
import com.supplier.exception.ProductNotFound;
import com.supplier.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService service;

    @GetMapping
    public List<ProductResponse> getProducts() {
        return service.getAll();
    }

    @GetMapping("/{productId}")
    public ProductResponse getProductById(@PathVariable Long productId) {
        return service.getById(productId);
    }

    @PostMapping
    public HrefResponse createProduct(@Valid  @RequestBody CreateProductRequest request) {
        return HrefResponse.of(service.create(request));
    }

    @PatchMapping("/{productId}")
    public HrefResponse updateProduct(@PathVariable Long productId, @RequestBody UpdateProductRequest request){
        return HrefResponse.of(service.update(productId, request));
    }

    @DeleteMapping("/{productId}")
    public void deleteProduct(@PathVariable Long productId) throws ProductNotFound {
        service.delete(productId);
    }

    @PatchMapping("/{productId}/quantity/{quantity}")
    public void updateQuantity(@PathVariable Long productId, @PathVariable Long quantity) {
        service.updateQuantity(productId, quantity);
    }
}
