package com.supplier.controller.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SupplierResponse {

    private Long id;

    private String name;

    private String createDate;

    private String updateDate;
}
