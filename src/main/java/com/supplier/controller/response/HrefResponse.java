package com.supplier.controller.response;

import com.supplier.entity.Product;
import lombok.Data;

@Data
public class HrefResponse {

    private Long id;

    private String href;

    public static HrefResponse of(Product product) {
        HrefResponse response = new HrefResponse();
        response.setId(product.getId());
        response.setHref("localhost:8080/product/"+product.getId());
        return response;
    }
}