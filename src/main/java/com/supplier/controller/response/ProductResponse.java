package com.supplier.controller.response;

import com.supplier.entity.Product;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ProductResponse {

    private Long id;

    private String name;

    private Long quantityInStock;

    private BigDecimal unitPrice;

    private SupplierResponse supplier;

    private String createDate;

    private String updateDate;

    public static ProductResponse of(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .quantityInStock(product.getQuantityInStock())
                .unitPrice(product.getUnitPrice())
                .supplier(SupplierResponse.builder()
                        .id(product.getSupplier().getId())
                        .name(product.getSupplier().getName())
                        .createDate(product.getSupplier().getCreateDate().toString())
                        .updateDate(product.getSupplier().getUpdateDate().toString())
                        .build())
                .createDate(product.getCreateDate().toString())
                .updateDate(product.getUpdateDate().toString())
                .build();
    }
}
