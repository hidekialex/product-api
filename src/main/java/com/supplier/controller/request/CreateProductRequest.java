package com.supplier.controller.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CreateProductRequest {

    @NotBlank
    private String name;

    @NotNull
    private Long quantityInStock;

    @NotNull
    private BigDecimal unitPrice;

    @NotNull
    private Long supplierId;
}
