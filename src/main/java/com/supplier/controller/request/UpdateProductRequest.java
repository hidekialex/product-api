package com.supplier.controller.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UpdateProductRequest {

    private String name;

    private BigDecimal unitPrice;
}
