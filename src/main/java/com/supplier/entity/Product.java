package com.supplier.entity;

import com.supplier.controller.request.CreateProductRequest;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity_in_stock")
    private Long quantityInStock;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="supplier_id", nullable=false)
    private Supplier supplier;

    @Column(name = "date_of_creation")
    private LocalDateTime createDate;

    @Column(name = "date_of_the_last_update")
    private LocalDateTime updateDate;

    public static Product of(CreateProductRequest request) {
        Product product = new Product();
        product.setName(request.getName());
        product.setQuantityInStock(request.getQuantityInStock());
        product.setUnitPrice(request.getUnitPrice());
        product.setUpdateDate(LocalDateTime.now());
        product.setCreateDate(LocalDateTime.now());

        Supplier supplier = new Supplier();
        supplier.setId(request.getSupplierId());
        product.setSupplier(supplier);
        return product;
    }
}