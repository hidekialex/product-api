package com.supplier.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "date_of_creation")
    private LocalDateTime createDate;

    @Column(name = "date_of_the_last_update")
    private LocalDateTime updateDate;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="supplier")
    private List<Product> products;
}