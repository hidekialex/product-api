CREATE TABLE `supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_of_creation` datetime(6) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_of_the_last_update` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_of_creation` datetime(6) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity_in_stock` bigint(20) DEFAULT NULL,
  `unit_price` decimal(19,2) DEFAULT NULL,
  `date_of_the_last_update` datetime(6) DEFAULT NULL,
  `supplier_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;