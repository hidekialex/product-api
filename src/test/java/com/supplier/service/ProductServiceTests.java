package com.supplier.service;

import com.supplier.controller.request.CreateProductRequest;
import com.supplier.controller.request.UpdateProductRequest;
import com.supplier.controller.response.ProductResponse;
import com.supplier.entity.Product;
import com.supplier.entity.Supplier;
import com.supplier.exception.ProductNotFound;
import com.supplier.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ProductServiceTests {

    @InjectMocks
    private ProductService service;

    @Mock
    private ProductRepository repository;

    @Captor
    private ArgumentCaptor<Product> productCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllIfRepositoryReturnsEmpty() {

        when(repository.findAll()).thenReturn(Collections.emptyList());
        List<ProductResponse> response = service.getAll();

        assertThat(response.isEmpty()).isTrue();

        verify(repository, times(1)).findAll();
    }

    @Test
    public void getAll() {

        Supplier supplier1 = new Supplier();
        supplier1.setId(100L);
        Supplier supplier2 = new Supplier();
        supplier2.setId(200L);
        Product product1 = new Product();
        product1.setId(1L);
        product1.setName("product1");
        product1.setSupplier(supplier1);
        Product product2 = new Product();
        product2.setId(2L);
        product2.setName("product2");
        product2.setSupplier(supplier2);
        when(repository.findAll()).thenReturn(Arrays.asList(product1, product2));

        List<ProductResponse> response = service.getAll();

        assertThat(response.get(0).getId()).isEqualTo(1L);
        assertThat(response.get(0).getName()).isEqualTo("product1");
        assertThat(response.get(0).getSupplier().getId()).isEqualTo(100L);
        assertThat(response.get(1).getId()).isEqualTo(2L);
        assertThat(response.get(1).getName()).isEqualTo("product2");
        assertThat(response.get(1).getSupplier().getId()).isEqualTo(200L);
        verify(repository, times(1)).findAll();
    }

    @Test
    public void getByIdIfProductDoesntExists() {

        Long productId = 100L;
        when(repository.findById(eq(productId))).thenReturn(Optional.empty());

        try {
            service.getById(productId);
            assertTrue(false);
        } catch(Exception e) {
            assertThat(e).isExactlyInstanceOf(ProductNotFound.class);
            assertThat(e.getMessage()).isEqualTo("Product not found.");
        }
        verify(repository, times(1)).findById(eq(productId));
    }

    @Test
    public void getById() {

        Long productId = 999L;
        Supplier supplier1 = new Supplier();
        supplier1.setId(100L);
        Product product1 = new Product();
        product1.setId(1L);
        product1.setName("product1");
        product1.setSupplier(supplier1);
        when(repository.findById(eq(productId))).thenReturn(Optional.of(product1));

        ProductResponse response = service.getById(productId);

        assertThat(response.getId()).isEqualTo(1L);
        assertThat(response.getName()).isEqualTo("product1");
        assertThat(response.getSupplier().getId()).isEqualTo(100L);
        verify(repository, times(1)).findById(eq(productId));
    }

    @Test
    public void create() {

        CreateProductRequest request = new CreateProductRequest();
        request.setName("product1");
        request.setQuantityInStock(10L);
        request.setUnitPrice(new BigDecimal(10.5));
        request.setSupplierId(1L);

        service.create(request);

        verify(repository, times(1)).save(productCaptor.capture());
        Product productCaptured = productCaptor.getValue();
        assertThat(productCaptured.getName()).isEqualTo("product1");
        assertThat(productCaptured.getQuantityInStock()).isEqualTo(10L);
        assertThat(productCaptured.getUnitPrice()).isEqualTo(new BigDecimal(10.5));
        assertThat(productCaptured.getSupplier().getId()).isEqualTo(1L);
    }

    @Test
    public void updateifProductDoesntExists() {

        Long productId = 100L;
        UpdateProductRequest request = new UpdateProductRequest();
        request.setName("product1");
        when(repository.findById(eq(productId))).thenReturn(Optional.empty());

        try {
            service.update(productId, request);
            assertTrue(false);
        } catch(Exception e) {
            assertThat(e).isExactlyInstanceOf(ProductNotFound.class);
            assertThat(e.getMessage()).isEqualTo("Product doesnt exists.");
        }

        verify(repository, times(1)).findById(eq(productId));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void update() {

        Long productId = 100L;
        UpdateProductRequest request = new UpdateProductRequest();
        request.setName("product1");
        request.setUnitPrice(new BigDecimal(999.9));
        Product product = new Product();
        product.setName("old product name");
        product.setUnitPrice(new BigDecimal(10.0));
        when(repository.findById(eq(productId))).thenReturn(Optional.of(product));

        service.update(productId, request);

        verify(repository, times(1)).findById(eq(productId));
        verify(repository, times(1)).save(productCaptor.capture());
        Product productCaptured = productCaptor.getValue();
        assertThat(productCaptured.getName()).isEqualTo("product1");
        assertThat(productCaptured.getUnitPrice()).isEqualTo(new BigDecimal(999.9));
        assertThat(productCaptured.getUpdateDate()).isNotNull();
    }

    @Test
    public void deleteIfProductDoesntExists() {

        Long productId = 100L;

        when(repository.existsById(eq(productId))).thenReturn(Boolean.FALSE);
        try {
            service.delete(productId);
            assertTrue(false);
        } catch(Exception e) {
            assertThat(e).isExactlyInstanceOf(ProductNotFound.class);
            assertThat(e.getMessage()).isEqualTo("Product doesnt exists.");
        }

        verify(repository, times(1)).existsById(eq(productId));
        verify(repository, times(0)).deleteById(any());
    }

    @Test
    public void delete() {

        Long productId = 1000L;
        when(repository.existsById(eq(productId))).thenReturn(Boolean.TRUE);

        service.delete(productId);

        verify(repository, times(1)).deleteById(eq(productId));
    }

    @Test
    public void updateQuantityIfProductDoesntExists() {

        Long productId = 100L;
        Long quantity = 2000L;

        try {
            service.updateQuantity(productId, quantity);
            assertTrue(false);
        } catch(Exception e) {
            assertThat(e).isExactlyInstanceOf(ProductNotFound.class);
            assertThat(e.getMessage()).isEqualTo("Product doesnt exists.");
        }

        verify(repository, times(1)).findById(eq(productId));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void updateQuantity() {

        Long productId = 100L;
        Long quantity = 2000L;
        Product product = new Product();
        product.setQuantityInStock(1000L);
        when(repository.findById(eq(productId))).thenReturn(Optional.of(product));

        service.updateQuantity(productId, quantity);

        verify(repository, times(1)).findById(eq(productId));
        verify(repository, times(1)).save(productCaptor.capture());
        Product productCaptured = productCaptor.getValue();
        assertThat(productCaptured.getQuantityInStock()).isEqualTo(quantity);
    }
}
