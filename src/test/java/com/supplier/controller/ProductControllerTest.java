package com.supplier.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.supplier.controller.request.CreateProductRequest;
import com.supplier.controller.request.UpdateProductRequest;
import com.supplier.controller.response.ProductResponse;
import com.supplier.entity.Product;
import com.supplier.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest {

    @InjectMocks
    private ProductController controller;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private ProductService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getProducts() throws Exception {

        ProductResponse productReponse1 = ProductResponse.builder()
                .id(1L)
                .name("product1")
                .build();

        ProductResponse productReponse2 = ProductResponse.builder()
                .id(2L)
                .name("product2")
                .build();

        when(service.getAll()).thenReturn(Arrays.asList(productReponse1, productReponse2));

        this.mockMvc.perform(get("/product")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("product1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("product2")));
    }

    @Test
    public void getProductById() throws Exception {

        Long productId = 1L;

        ProductResponse productReponse1 = ProductResponse.builder()
                .id(productId)
                .name("product1")
                .build();

        when(service.getById(eq(productId))).thenReturn(productReponse1);

        this.mockMvc.perform(get("/product/{productId}", productId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("product1")));
    }

    @Test
    public void createProduct() throws Exception {

        Long productId = 1L;

        CreateProductRequest request = new CreateProductRequest();
        request.setName("product1");
        request.setQuantityInStock(200L);
        request.setUnitPrice(new BigDecimal(120.0));
        request.setSupplierId(2L);

        Product product = new Product();
        product.setId(productId);

        when(service.create(any(CreateProductRequest.class))).thenReturn(product);

        this.mockMvc.perform(post("/product")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.href", is("localhost:8080/product/1")));
    }

    @Test
    public void updateProduct() throws Exception {

        Long productId = 1L;

        UpdateProductRequest request = new UpdateProductRequest();
        request.setName("product1");
        request.setUnitPrice(new BigDecimal(120.0));

        Product product = new Product();
        product.setId(productId);

        when(service.update(eq(productId), any(UpdateProductRequest.class))).thenReturn(product);

        this.mockMvc.perform(patch("/product/{productId}", productId)
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.href", is("localhost:8080/product/1")));
    }

    @Test
    public void deleteProduct() throws Exception {

        Long productId = 1L;
        doNothing().when(service).delete(eq(productId));

        this.mockMvc.perform(delete("/product/{productId}", productId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateQuantity() throws Exception {

        Long productId = 1L;
        Long quantity = 200L;

        doNothing().when(service).updateQuantity(eq(productId), eq(quantity));

        this.mockMvc.perform(patch("/product/{productId}/quantity/{quantityId}", productId, quantity)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}