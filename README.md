### Instruções para rodar o projeto

Certifique-se que tenha instalado na maquina as seguintes ferramentas:

* Java (Runtime)
* Maven
* Docker

Certifique-se que o seu usuario do SO tenha permissão para executar o script init.sh localizado na raiz do projeto, e que também possa executar comandos do docker.

Execute o seguinte comando na raiz do projeto:

    $ ./init.sh

Após subir o projeto é possível acessar a documentação em: http://localhost:8080/swagger-ui.html#

Para executar/testar as chamadas é só importar a collection para o postman que se localiza na raiz do projeto: 

    /product.postman_collection.json

Os scripts de criação e inserção são executados pelo **flyway** ao inicializar o projeto e encontram-se no diretório:

    /src/main/resources/db/migration
